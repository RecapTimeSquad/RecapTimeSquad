# Hi there 👋

This is the placeholder account for [the Recap Time Squad](https://github.com/RecapTime). You can use this placeholder to mention us in issues
or in pull requests, and a team member will respond as soon as possible, though our humans have other business outside GitHub/GitLab SaaS.

## Team Members
When mentioned, one of our team members listed below will respond to issues and pull requests as soon as possible. In case no response has been
received yet, you can probably mention them (but don't abusively ping them every minute/hour) and patiently wait.

* Andrei Jiroh - [@ajhalili2006](https://github.com/ajhalili2006)
* Your name here, you probably want to join us?

## Contacting us directly

This list is probably go outdated soon, so [see our contact page for up-to-date list](https://madebythepins.tk/contact).

* We're available through email at <yourfriends@recaptime.tk> (<hi@recaptime.dev> in the distant future, probably)
* DMs are open on [Twitter](https://twitter.com/ThePinsTeam) and [Telegram](https://t.me/ThePinsTeam)
